# Lithuanian translations for l package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
#
# Automatically generated, 2014.
# Liudas Ališauskas <liudas@aksioma.lt>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2013-07-05 01:36+0000\n"
"PO-Revision-Date: 2014-03-15 02:13+0200\n"
"Last-Translator: Liudas Ališauskas <liudas@aksioma.lt>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Lokalize 1.5\n"

#: daemon.cpp:55
msgid "Switch Display"
msgstr "Pakeisti monitorių"